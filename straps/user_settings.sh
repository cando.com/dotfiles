user_settings(){
  dotfiles=$1

  gsettings set org.gnome.settings-daemon.peripherals.keyboard delay 200
  sudo cp $dotfiles/candoer.user /var/lib/AccountsService/users/candoer
  sudo cp $dotfiles/candoer.icon /var/lib/AccountsService/icons/candoer
}

export -f user_settings
