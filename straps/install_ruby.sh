install_ruby(){
  if ! chruby ruby-2.3.1 2> /dev/null; then
    sudo apt-get install -y build-essential libffi-dev libgdbm-dev libncurses5-dev libreadline-dev libssl-dev libyaml-dev zlib1g-dev

    wget -O chruby-0.3.9.tar.gz https://github.com/postmodern/chruby/archive/v0.3.9.tar.gz
    tar -xzvf chruby-0.3.9.tar.gz
    pushd chruby-0.3.9/
    sudo make install
    popd > /dev/null

    git clone https://github.com/rbenv/ruby-build.git
    pushd ruby-build
    sudo ./install.sh
    sudo ruby-build 2.3.1 /opt/rubies/ruby-2.3.1.
    popd > /dev/null
  fi
}

export -f install_ruby
