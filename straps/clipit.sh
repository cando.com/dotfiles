clipit(){
  home=$1

  mkdir -p "$home/.config/autostart"
  cp /usr/share/applications/clipit.desktop ~/.config/autostart/clipit.desktop
}

export -f clipit
