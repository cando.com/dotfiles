cloud_sdk(){
  if [ ! -e "/usr/local/google-cloud-sdk" ]; then
    wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-109.0.0-linux-x86_64.tar.gz
    tar -xzvf google-cloud-sdk-109.0.0-linux-x86_64.tar.gz
    sudo rsync -a google-cloud-sdk /usr/local/
    /usr/local/google-cloud-sdk/install.sh -q
  fi
}

export -f cloud_sdk
