setup_environment() {
  sudo apt-get -y update
  sudo apt-get -y upgrade
  sudo apt-get -y install git realpath
}

export -f setup_environment
