install_kubectl() {
	if ! which kubectl; then
		sudo /usr/local/google-cloud-sdk/bin/gcloud components install kubectl -q
	fi
}

export -f install_kubectl
