toolchain_packages(){
  sudo apt-get install -y tmux xsel trash-cli clipit
}

export -f toolchain_packages
