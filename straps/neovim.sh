install_neovim(){
  sudo apt-get install -y exuberant-ctags ncurses-term python-dev python-pip python3-dev python3-pip software-properties-common
  sudo pip install neovim
  if ! which nvim; then
    sudo add-apt-repository -y ppa:neovim-ppa/unstable
    sudo apt-get -y update
    sudo apt-get install -y neovim
  fi
}

export -f install_neovim
