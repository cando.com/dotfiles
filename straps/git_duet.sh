git_duet(){
  if ! which git-duet > /dev/null; then
    GOVENDOREXPERIMENT=1 go get github.com/git-duet/git-duet/...
  fi
}

export -f git_duet
