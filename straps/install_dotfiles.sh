install_dotfiles(){
  home=$1

  if [ ! -e "$home/.homesick/repos/dotfiles" ]; then
    homeshick --batch clone http://gitlab.com/cando.com/dotfiles.git
  else
    homeshick pull dotfiles
  fi

  homeshick link --force
  source ~/.profile
}

export -f install_dotfiles
