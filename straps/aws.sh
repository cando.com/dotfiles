install_aws(){
  sudo apt-get install -y python-pip
  sudo pip install awscli awsebcli
}

export -f install_aws
