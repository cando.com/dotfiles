install_docker(){
  if ! which docker; then
    sudo apt-get install -y apparmor
    sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
    sudo apt-get update
    apt-cache policy docker-engine
    sudo apt-get install -y docker-engine
    sudo docker run hello-world
  fi

  # docker-compose
  if ! which docker-compose; then
    sudo curl -L https://github.com/docker/compose/releases/download/1.7.0/docker-compose-`uname -s`-`uname -m` > docker-compose
    sudo install docker-compose /usr/local/bin/docker-compose
  fi

  # docker as non root
  sudo groupadd docker -f
  sudo gpasswd -a ${USER} docker
}

export -f install_docker
