install_atom(){
  if ! dpkg -p atom | grep 1.7.4; then
    wget https://github.com/atom/atom/releases/download/v1.7.4/atom-amd64.deb
    sudo dpkg -i atom-amd64.deb
  fi
  apm install react
}

export -f install_atom
