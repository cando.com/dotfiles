git_prompt(){
  home=$1
  if [ ! -e "$home/.bash-git-prompt" ]; then
    git clone https://github.com/magicmonty/bash-git-prompt.git "$home/.bash-git-prompt" --depth=1
  fi
}

export -f git_prompt
