install_etcd(){
  if ! which etcd; then
    curl -L  https://github.com/coreos/etcd/releases/download/v2.3.4/etcd-v2.3.4-linux-amd64.tar.gz -o etcd-v2.3.4-linux-amd64.tar.gz
    tar xzvf etcd-v2.3.4-linux-amd64.tar.gz
    sudo install ./etcd-v2.3.4-linux-amd64/etcd /usr/local/bin
  fi
}

export -f install_etcd
