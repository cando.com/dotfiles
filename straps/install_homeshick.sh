install_homeshick(){
  home=$1
  if [ ! -e "$home/.homesick/repos/homeshick" ]; then
    git clone git://github.com/andsens/homeshick.git $home/.homesick/repos/homeshick
  fi
  source $home/.homesick/repos/homeshick/homeshick.sh
}

export -f install_homeshick
