go(){
  if [ ! -e "/usr/local/go" ]; then
    wget https://storage.googleapis.com/golang/go1.6.2.linux-amd64.tar.gz
    tar -xvzf go*.linux-amd64.tar.gz
    sudo rsync -a go /usr/local
  fi

}

export -f go
