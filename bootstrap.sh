#!/bin/bash
set -eux
source ~/.profile

dotfiles=$(dirname $(realpath $0))
workspace="/tmp/bootstrap-$(date +%s)"
mkdir -p $workspace

for f in straps/*; do source $f; done

pushd $workspace > /dev/null
  check_for_internet
  setup_environment
  update_kernel
  toolchain_packages
  clipit $HOME
  user_settings $dotfiles
  chrome
  install_atom
  install_node
  install_aws
  install_neovim
  install_docker
  git_prompt $HOME
  cloud_sdk
  install_kubectl
  install_etcd
  install_homeshick $HOME
  trust_github $HOME
  install_dotfiles $HOME
  git_duet
  cleanup_packages
popd > /dev/null

echo "Ready to go! You may want to reboot or re-bash."
